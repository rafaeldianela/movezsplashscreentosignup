package com.example.u_hop.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.WindowManager;

public class SplashScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



        setContentView(R.layout.activity_splash);

        LogoLauncher logoLauncher = new LogoLauncher();
        logoLauncher.start();

    }


    private class LogoLauncher extends Thread{
        public void run(){
            try{

                sleep(3500);

            }
            catch(InterruptedException e){
                e.printStackTrace();
            }
                Intent intent = new Intent(SplashScreen.this, Login.class);
                startActivity(intent);
                SplashScreen.this.finish();

        }



    }
}
